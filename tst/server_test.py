import unittest

import spizh.server as spizh

from flask_testing import TestCase


class ServerTestCase(TestCase):
    def create_app(self):
        self.app = spizh.app
        self.app.testing = True
        self.app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///:memory:"
        self.app.config['PRESERVE_CONTEXT_ON_EXCEPTION'] = False

        return self.app

    def setUp(self):
        self.app_client = self.app.test_client()
        spizh.db.create_all()
        self.addCleanup(self.tearDown)

    def tearDown(self):
        spizh.db.session.remove()
        spizh.db.drop_all()


if __name__ == '__main__':
    unittest.main()
