import base64
import copy
import unittest
import urllib.parse

from api import api_v1
from spizh.db.model.user import User
from spizh.server import db
from tst.server_test import ServerTestCase

class ApiTestCase(ServerTestCase):
    def setUp(self):
        ServerTestCase.setUp(self)

        self.api_version = api_v1

        self.dummy_user_info = {"name": "Witold", "auth": "foo"}
        self.dummy_user = User(**self.dummy_user_info)
        db.session.add(self.dummy_user)
        db.session.commit()

    def send_request(self, request_params, fail_on_404=True):
        with self.app.test_request_context():
            request_params["path"] = urllib.parse.quote(request_params["path"])
            response = self.app_client.open(**request_params)

        if fail_on_404 and response.status_code == 404:
            self.fail(
                "Request to %s resulted in 404. You have probably broken the API for this op" % request_params['path'])

        return response

    def tearDown(self):
        ServerTestCase.tearDown(self)


class ProtectedApiTestCase(ApiTestCase):
    def setUp(self):
        ApiTestCase.setUp(self)

    def test_request_without_authorization_fails(self):
        if hasattr(self, "request_params"):
            with self.app.test_request_context():
                response = self.send_request(self.request_params)
            self.assertEqual(response.status_code, 401)
        else:
            self.skipTest("has no request_params")

    def make_authorization_header(self, name, auth):
        auth_string = base64.b64encode(bytes("{name}:{auth}".format(**locals()), "ASCII"))
        return {"Authorization": "Basic %s" % auth_string.decode("ascii")}

    def send_authorized_request(self, request_params, user_credentials, fail_on_404=True):
        authorized_params = copy.deepcopy(request_params)
        authorized_params["headers"] = self.make_authorization_header(user_credentials["name"],
                                                                      user_credentials["auth"])
        return self.send_request(authorized_params, fail_on_404)

    def tearDown(self):
        ApiTestCase.tearDown(self)

if __name__ == '__main__':
    unittest.main()
