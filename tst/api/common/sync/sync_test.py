from uuid import uuid4

from flask import json
from parameterized import parameterized
from tst.api.common.commons import ProtectedApiTestCase

from spizh.api import api_v1
from spizh.db.model.clientinstance import ClientInstance
from spizh.db.model.product import Product
from spizh.extensions import db
from tst.api.common.api_helpers import DeviceRequestHelpers, ProductRequestHelpers, DBHelpers


class BaseSyncTest(ProtectedApiTestCase, DeviceRequestHelpers, ProductRequestHelpers):
    def setUp(self):
        ProtectedApiTestCase.setUp(self)

        self.dummy_device_ids = ["ANDROID-C0FFEE", "ANDROID-DEADBEEF"]
        for device_id in self.dummy_device_ids:
            self.register_new_device(device_id, self.dummy_user.uuid, self.dummy_user_info)
            self.dummy_device = ClientInstance.query.filter_by(device_id=device_id,
                                                               user_id=self.dummy_user.uuid) \
                .first()

        self.products = [Product(name="a", price=1, store="x"),
                         Product(name="b", price=2, store="x")]
        for idx, product in enumerate(self.products):
            product_info = self.send_product_insert_request(product.to_dict(),
                                                            self.dummy_device_ids[0],
                                                            self.dummy_user_info)

    def make_incoming_updates_json(self, new_products: list, quantity_updates: list, removed_products: list=None):
        """
        incoming updates dict schema:

        :param new_products:
        :param quantity_updates:
        :return:
        """
        updates_json = {}
        if new_products:
            updates_json["product"] = {"added": new_products}
        if removed_products:
            updates_json["product"] = {"removed": removed_products}
        if quantity_updates:
            updates_json["quantity"] = quantity_updates
        return updates_json

    def make_single_incoming_quantity_json(self, product_id, delta):
        return {"product_id": product_id,
                "delta": delta}

    def send_sync_request(self, device_id, updates_json, user_credentials):
        self.tested_action_uri = api_v1.url_prefix + '/sync'
        self.request_json = updates_json
        if device_id:
            self.request_json["device_id"] = device_id

        self.request_params = {"method": "POST",
                               "path": self.tested_action_uri,
                               "content_type": "application/json",
                               "data": json.dumps(self.request_json)}
        return self.send_authorized_request(self.request_params, user_credentials)

    def setup_test_db(self):
        # 2 users
        # 2 devices per user
        # 3 products per user
        #   * one shared, between both devices and the server
        #   * one synced with the server but not the other device
        #   * one unsynced

        # quantity info


        raise NotImplementedError


class TestBehaviourOnIncompleteIncomingSyncBundle(BaseSyncTest):
    def setUp(self):
        BaseSyncTest.setUp(self)

        product_updates = [{"uuid": str(uuid4()),
                            "name": "smalec",
                            "price": 0.98,
                            "store": "targ"}]
        quantity_updates = {self.products[0].uuid: 5}
        self.updates_json = self.make_incoming_updates_json(product_updates, quantity_updates)

    @parameterized.expand([
        ('product updates', 'product'),
        ('quantity updates', 'quantity')
        ])
    def test_no_error_code_on_missing(self, _, key_to_remove):
        self.updates_json.pop(key_to_remove)
        response = self.send_sync_request(self.dummy_device_ids[0], self.updates_json, self.dummy_user_info)

        self.assertFalse(self.is_client_error_status(response.status_code),
                         msg=str(response.status_code) + str(response.data))
        self.assertFalse(self.is_server_error_status(response.status_code),
                         msg=str(response.status_code) + str(response.data))

    def test_empty_sync_request_doesnt_cause_an_error(self):
        response = self.send_sync_request(self.dummy_device_ids[0], {}, self.dummy_user_info)

        self.assertFalse(self.is_client_error_status(response.status_code), msg=response.status_code)
        self.assertFalse(self.is_server_error_status(response.status_code), msg=response.status_code)

    @staticmethod
    def is_informational_status(status_code):
        return str(status_code).startswith('1')

    @staticmethod
    def is_succesful_status(status_code):
        return str(status_code).startswith('2')

    @staticmethod
    def is_redirection_status(status_code):
        return str(status_code).startswith('3')

    @staticmethod
    def is_client_error_status(status_code):
        return str(status_code).startswith('4')

    @staticmethod
    def is_server_error_status(status_code):
        return str(status_code).startswith('5')

    def tearDown(self):
        super().tearDown()


class TestSyncFailsOnUnknownDevice(BaseSyncTest):
    def setUp(self):
        super().setUp()

    def test_412_if_device_not_in_database(self):
        response = self.send_sync_request("ANDROID-BOGUS", {"foo": "bar"}, self.dummy_user_info)
        self.assertEqual(response.status_code, 412)

    def tearDown(self):
        super().tearDown()


class TestSyncFailsOnUnknownProducts(BaseSyncTest):
    def setUp(self):
        BaseSyncTest.setUp(self)

        quantity_updates = {str(uuid4()): 5}
        self.updates_json = self.make_incoming_updates_json(None, quantity_updates)
        self.response = self.send_sync_request(self.dummy_device_ids[0], self.updates_json, self.dummy_user_info)

    def test_406_if_product_not_in_database(self):
        self.assertEqual(self.response.status_code, 406)


class TestNewProductSync(BaseSyncTest, DBHelpers):
    def setUp(self):
        super().setUp()

        preinserted_product = Product(name="chleb", price=1.23, store="Tesco")
        dev_id, dev_id2 = "ANDROID-DEAFBEEF", "ANDROID_COFFEE"

        dev = DeviceRequestHelpers.register_new_device(self, dev_id, self.dummy_user.uuid, self.dummy_user_info)
        dev2 = DeviceRequestHelpers.register_new_device(self, dev_id2, self.dummy_user.uuid, self.dummy_user_info)
        preinserted_product = ProductRequestHelpers.send_product_insert_request(self, preinserted_product.to_dict(),
                                                                                dev_id,
                                                                                self.dummy_user_info)
        ProductRequestHelpers.send_product_update(self, preinserted_product["id"], 2, dev_id, self.dummy_user_info)
        ProductRequestHelpers.send_product_update(self, preinserted_product["id"], 1, dev_id, self.dummy_user_info)
        ProductRequestHelpers.send_product_update(self, preinserted_product["id"], 3, dev_id2, self.dummy_user_info)

        self.new_product = Product(name="kasza", price=0.66, store="Tesco")
        self.new_products = [self.new_product]
        self.synced_products = [product.to_dict() for product in self.new_products]
        self.updates_json = self.make_incoming_updates_json(self.synced_products, None)

        self.product_to_sync_query = db.session.query(Product).filter_by(name=self.new_product.name,
                                                                         price=self.new_product.price,
                                                                         store=self.new_product.store)
        self.assertIsNone(self.product_to_sync_query.first())

        self.response = self.send_sync_request(dev_id2, self.updates_json, self.dummy_user_info)

    def test_product_was_added(self):
        product = self.product_to_sync_query.first()
        self.assertIsNotNone(product)
        self.assertEqual(product.uuid, self.new_product.uuid)
        self.assertEqual(product.name, self.new_product.name)
        self.assertEqual(float(product.price), self.new_product.price)
        self.assertEqual(product.store, self.new_product.store)

    def test_product_has_no_quantity_info(self):
        for product in self.new_products:
            self.assertIsNone(self.get_product_quantity(product.uuid).one_or_none())

    def test_product_ids_are_returned(self):
        self.skipTest("No longer relevant")
        json = self.response.json
        product_id_mapping = json["product_id_mapping"]
        self.assertEqual(len(product_id_mapping), 1)

    def test_new_product_ids_map_to_proper_products(self):
        self.skipTest("No longer relevant")
        json = self.response.json
        product_id_mapping = json["product_id_mapping"]
        for tmp_id, id in product_id_mapping.items():
            self.assertDictEqual(db.session.query(Product).get(id).to_dict(),
                                 self.products[tmp_id])

class TestRemovedProductSync(BaseSyncTest, DBHelpers):
    def setUp(self):
        super().setUp()

        self.preinserted_product = Product(name="chleb", price=1.23, store="Tesco")
        dev_id, dev_id2 = "ANDROID-DEAFBEEF", "ANDROID_COFFEE"

        dev = DeviceRequestHelpers.register_new_device(self, dev_id, self.dummy_user.uuid, self.dummy_user_info)
        dev2 = DeviceRequestHelpers.register_new_device(self, dev_id2, self.dummy_user.uuid, self.dummy_user_info)
        self.preinserted_product_info = ProductRequestHelpers.send_product_insert_request(self, self.preinserted_product.to_dict(),
                                                                                dev_id,
                                                                                self.dummy_user_info)
        ProductRequestHelpers.send_product_update(self, self.preinserted_product_info["id"], 2, dev_id, self.dummy_user_info)
        ProductRequestHelpers.send_product_update(self, self.preinserted_product_info["id"], 1, dev_id, self.dummy_user_info)
        ProductRequestHelpers.send_product_update(self, self.preinserted_product_info["id"], 3, dev_id2, self.dummy_user_info)

        self.updates_json = self.make_incoming_updates_json(None, None, [self.preinserted_product_info["id"]])

        self.removed_product_query = db.session.query(Product).filter_by(name=self.preinserted_product.name,
                                                                         price=self.preinserted_product.price,
                                                                         store=self.preinserted_product.store)

        self.removed_product_quantities_query = DBHelpers.get_product_quantity(self, self.preinserted_product_info["id"])

        self.assertIsNotNone(self.removed_product_query.first())
        self.assertEqual(self.removed_product_quantities_query.scalar(), 6)

        self.response = self.send_sync_request(dev_id2, self.updates_json, self.dummy_user_info)

    def test_product_is_still_in_database(self):
        self.assertIsNotNone(self.removed_product_query.first())

    def test_product_quantity_info_is_retained(self):
        self.assertIsNotNone(self.removed_product_quantities_query.scalar())

    def test_product_is_marked_as_deleted(self):
        self.assertTrue(self.removed_product_query.first().is_removed)


class TestQuantitySync(BaseSyncTest, DBHelpers):
    def setUp(self):
        super().setUp()

        preinserted_product = Product(name="chleb", price=1.23, store="Tesco")
        dev_id = ["ANDROID-DEAFBEEF", "ANDROID_COFFEE", "ANDROID-F001"]

        for id in dev_id:
            DeviceRequestHelpers.register_new_device(self, id, self.dummy_user.uuid, self.dummy_user_info)

        self.preinserted_product = ProductRequestHelpers.send_product_insert_request(self,
                                                                                     preinserted_product.to_dict(),
                                                                                     dev_id[0],
                                                                                     self.dummy_user_info)

        ProductRequestHelpers.send_product_update(self, self.preinserted_product["id"], 2, dev_id[0],
                                                  self.dummy_user_info)
        ProductRequestHelpers.send_product_update(self, self.preinserted_product["id"], 3, dev_id[1],
                                                  self.dummy_user_info)
        ProductRequestHelpers.send_product_update(self, self.preinserted_product["id"], 1, dev_id[2],
                                                  self.dummy_user_info)

        self.updates_json = self.make_incoming_updates_json(None, {self.preinserted_product["id"]: 10})

        self.query_product_qty = DBHelpers.get_product_quantity(self, self.preinserted_product["id"])

        self.assertEqual(self.query_product_qty.scalar(), 6)

        self.response = self.send_sync_request(dev_id[0], self.updates_json, self.dummy_user_info)

    def test_product_quantity_is_updated(self):
        self.assertEqual(DBHelpers.get_product_quantity(self, self.preinserted_product["id"]).scalar(), 3 + 1 + 10)

    def test_device_receives_quantity_update_from_server(self):
        updates = self.response.json
        self.assertEqual(len(updates["quantity"]), 1)

    def test_quantity_update_from_server_is_aggregated_over_all_devices(self):
        updates = self.response.json
        self.assertEqual(updates["quantity"][str(self.preinserted_product["id"])], 4, msg=str(updates))

    def test_server_doesnt_send_id_mapping_if_client_uses_serverside_id(self):
        self.skipTest("product id remapping no longer used")
        self.assertIsNone(self.response.json["product_id_mapping"])

        # class TestOutgoingSyncBundle(BaseSyncV2Test):
