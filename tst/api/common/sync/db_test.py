from spizh.api.common.sync.db import query_quantity_of_products_not_known_by_client_instance, \
    query_aggregate_remote_quantity_updates, query_remote_products
from spizh.db.model.product import Product
from spizh.extensions import db
from tst.api.common.api_helpers import DeviceRequestHelpers, ProductRequestHelpers
from tst.api.common.sync.sync_test import BaseSyncTest


class TestRemoteQuantityQuery(BaseSyncTest, DeviceRequestHelpers, ProductRequestHelpers):
    def setUp(self):
        super().setUp()

        self.device_ids = ["ANDROID-DEAFBEEF", "ANDROID_COFFEE", "ANDROID-F001"]
        for id in self.device_ids:
            DeviceRequestHelpers.register_new_device(self, id, self.dummy_user.uuid, self.dummy_user_info)

        self.preinserted_product = Product(name="chleb", price=1.23, store="Tesco")
        ProductRequestHelpers.send_product_insert_request(self,
                                                          self.preinserted_product.to_dict(),
                                                          self.device_ids[0],
                                                          self.dummy_user_info)

        ProductRequestHelpers.send_product_update(self, self.preinserted_product.uuid, 2, self.device_ids[0],
                                                  self.dummy_user_info)
        ProductRequestHelpers.send_product_update(self, self.preinserted_product.uuid, 3, self.device_ids[1],
                                                  self.dummy_user_info)
        ProductRequestHelpers.send_product_update(self, self.preinserted_product.uuid, 5, self.device_ids[2],
                                                  self.dummy_user_info)

        self.remote_quantities_query = query_quantity_of_products_not_known_by_client_instance(self.dummy_user.uuid,
                                                                                               self.device_ids[0])
        # test with another product in the database

    def test_remote_quantities_query_returns_only_remote_quantities(self):
        remote_quantity_objects = db.session.query(self.remote_quantities_query).all()
        # results = db.session.query(subquery).all()
        self.assertEqual(len(remote_quantity_objects), 2)
        self.assertNotIn(self.device_ids[0], (r.device_id for r in remote_quantity_objects))

    def test_aggregate_remote_quantity_returns_an_aggregate(self):
        aggregates = query_aggregate_remote_quantity_updates(self.device_ids[0], self.dummy_user.uuid).all()
        self.assertEqual(len(aggregates), 1)
        self.assertEqual(aggregates[0].delta_sum, 8)
        self.assertEqual(aggregates[0].product_id, self.preinserted_product.uuid)


class TestRemoteProductQuery(BaseSyncTest, DeviceRequestHelpers, ProductRequestHelpers):
    def setUp(self):
        super().setUp()

        self.device_ids = ["ANDROID-DEAFBEEF", "ANDROID_COFFEE", "ANDROID-F001"]
        for id in self.device_ids:
            DeviceRequestHelpers.register_new_device(self, id, self.dummy_user.uuid, self.dummy_user_info)

        self.products = [Product(name="A", price=1.23, store="Tesco"),
                         Product(name="B", price=4.56, store="Tesco"),
                         Product(name="C", price=7.89, store="Tesco")]

        for idx, product in enumerate(self.products):
            ProductRequestHelpers.send_product_insert_request(self,
                                                              product.to_dict(),
                                                              self.device_ids[idx],
                                                              self.dummy_user_info)
            ProductRequestHelpers.send_product_update(self, product.uuid, 1, self.device_ids[idx],
                                                      self.dummy_user_info)

        self.remote_products_query = query_remote_products(self.device_ids[0], self.dummy_user.uuid)

    def test_remote_remote_products(self):
        remote_products = self.remote_products_query.all()

        self.assertEquals(len(remote_products), 2)
        self.assertSetEqual({'C', 'B'}, {product.name for product in remote_products})
