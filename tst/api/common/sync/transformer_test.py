from tst.api.common.api_helpers import DeviceRequestHelpers, ProductRequestHelpers
from tst.api.common.sync.sync_test import BaseSyncTest


class TestProductsUpdateBundleAssembler(BaseSyncTest, DeviceRequestHelpers, ProductRequestHelpers):
    def setUp(self):
        super().setUp()

    def test_bundle_schema_is_correct(self):
        self.skipTest('cutting down on time budget')