from tst.api.common.commons import ProtectedApiTestCase

from spizh.db.model.clientinstance import ClientInstance
from tst.api.common.api_helpers import DeviceRequestHelpers


class TestGetDeviceInfo(ProtectedApiTestCase, DeviceRequestHelpers):
    def setUp(self):
        super().setUp()
        self.dummy_device_id = "ANDROID-C0FFEE"
        self.register_new_device(self.dummy_device_id, self.dummy_user.uuid, self.dummy_user_info)

        self.response = self.get_device_info(self.dummy_device_id, self.dummy_user.uuid, self.dummy_user_info)

    def test_200_if_device_exists(self):
        self.assertEqual(self.response.status_code, 200)

    def test_404_if_device_does_not_exist(self):
        response = self.get_device_info("ANDROID-BOGUS", self.dummy_user.uuid, self.dummy_user_info)
        self.assertEqual(response.status_code, 404)

    def tearDown(self):
        super().tearDown()


class TestNewDeviceRegistration(ProtectedApiTestCase, DeviceRequestHelpers):
    def setUp(self):
        super().setUp()

        self.device_id = "ANDROID-C0FFEE"
        self.response = self.register_new_device(self.device_id, self.dummy_user.uuid, self.dummy_user_info)

    def test_device_is_added_on_valid_request(self):
        self.assertEqual(self.response.status_code, 202)
        device_instance = ClientInstance.query.filter_by(user_id=self.dummy_user.uuid,
                                                         device_id=self.device_id).first()

        self.assertIsNotNone(device_instance)

    def test_returns_417_when_device_id_is_missing(self):
        self.response = self.register_new_device(self.device_id, self.dummy_user.uuid, self.dummy_user_info, {})
        self.assertEqual(self.response.status_code, 417)

    def test_returns_403_when_other_user_is_accessed(self):
        self.response = self.register_new_device(self.device_id, 7447, self.dummy_user_info)
        self.assertEqual(self.response.status_code, 403)

    def tearDown(self):
        super().tearDown()
