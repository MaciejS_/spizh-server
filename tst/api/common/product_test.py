import random

from flask import jsonify
from parameterized import parameterized
from sqlalchemy.sql import label

from spizh.api.common.product import logic as product_api_logic
from spizh.db.model.clientinstance import ClientInstance
from spizh.db.model.product import Product
from spizh.db.model.quantity import Quantity
from spizh.db.model.user_product import UserProduct
from spizh.extensions import db
from tst.api.common.api_helpers import DeviceRequestHelpers, ProductRequestHelpers
from tst.api.common.commons import ProtectedApiTestCase, ApiTestCase


class ProductAPITestCase(ProtectedApiTestCase, ProductRequestHelpers, DeviceRequestHelpers):
    def setUp(self):
        ApiTestCase.setUp(self)

        self.dummy_device_id = "ANDROID-C0FFEE"
        self.dummy_client_instance = ClientInstance(self.dummy_device_id, self.dummy_user.uuid)
        db.session.add(self.dummy_client_instance)
        db.session.commit()

        self.expected_product = Product(name="herbatniki", price=1.41, store="Tesco")
        # self.expected_product = Product(**self.product_info)

        self.assertIsNotNone(self.expected_product.uuid)
        self.assertIsNone(Product.query.filter_by(name=self.expected_product.name).first())

        self.product_info = self.send_product_insert_request(self.expected_product.to_dict(), self.dummy_device_id,
                                                             self.dummy_user_info)

        self.assertTrue(self.product_info["uuid"], self.expected_product.uuid)
        self.query_results = Product.query.filter_by(name=self.product_info.get('name'))

    def tearDown(self):
        ApiTestCase.tearDown(self)


class TestValidProductInsert(ProductAPITestCase):
    def setUp(self):
        ProductAPITestCase.setUp(self)

    def test_product_was_actually_inserted(self):
        self.assertIsNotNone(self.query_results)
        self.assertEqual(len(self.query_results.all()), 1)

        actual_product = self.query_results.first()
        self.assertEqual(self.response.json['id'], actual_product.uuid)

    def test_product_is_linked_to_user(self):
        actual_product = self.query_results.first()
        user_product_mappings = UserProduct.query.filter_by(product_id=actual_product.uuid)

        mapping = user_product_mappings.first()
        self.assertEqual(mapping.user_id, self.dummy_user.uuid)
        self.assertEqual(mapping.product_id, actual_product.uuid)

    def test_freshly_inserted_product_has_no_quantity_info(self):
        quantity_query = db.session.query(Quantity) \
            .join(UserProduct) \
            .filter(UserProduct.product_id == self.product_info.get("id"))

        self.assertEqual(quantity_query.count(), 0)

    def test_server_response_is_correct(self):
        actual_product = self.query_results.first()
        expected_response = jsonify({"id": actual_product.uuid})
        expected_response.status_code = 201

        self.assertEqual(self.response.status_code, expected_response.status_code)
        self.assertEqual(expected_response.json, self.response.json)

    def tearDown(self):
        ProductAPITestCase.tearDown(self)


class TestGetListOfAllProductsForUser(ProductAPITestCase):
    def setUp(self):
        ProductAPITestCase.setUp(self)

        self.expected_other_product = Product(name="grzybki", price=4.20, store="las")

        self.assertIsNone(Product.query.filter_by(name=self.expected_other_product.name).first())

        self.other_product_info = self.send_product_insert_request(self.expected_other_product.to_dict(),
                                                                   self.dummy_device_id,
                                                                   self.dummy_user_info)

        self.device_ids = ["ANDROID-DEADBEEF"]
        for device_id in self.device_ids:
            self.register_new_device(device_id=device_id,
                                     user_id=self.dummy_user.uuid,
                                     user_credentials=self.dummy_user_info)
            # might fail here on user credentials unpacking

        # insert some quantities
        self.quantities = {}
        for product_id in [p["id"] for p in [self.product_info, self.other_product_info]]:
            for device_id in ["ANDROID-C0FFEE", "ANDROID-DEADBEEF"]:
                delta = random.randint(-5, 5)
                self.quantities[(product_id, device_id)] = delta
                self.send_product_update(product_id, delta, device_id, self.dummy_user_info)

        self.get_products_response = self.get_all_products_request(self.dummy_user_info)

    def test_single_product_info_has_all_required_keys(self):
        products_list = self.get_products_response.json

        expected_keys = ["name", "quantity", "price", "store"]
        _, product_info = random.choice(list(products_list.items()))

        self.assertSetEqual(set(expected_keys), set(product_info.keys()))

    def test_product_quantity_is_a_sum_of_related_Quantity_objects(self):
        products_list = self.get_products_response.json
        for product_id, product_info in products_list.items():
            expected_quantity = sum(qty for key, qty in self.quantities.items() if key[0] == product_id)
            self.assertEqual(product_info["quantity"], expected_quantity)

    def test_all_products_added_by_user_are_returned(self):
        products_list = self.get_products_response.json
        expected_products = {product['id']: product for product in [self.product_info, self.other_product_info]}

        self.assertEqual(len(products_list), 2)
        for id, product_info in products_list.items():
            self.assertIn(id, expected_products.keys())

    def tearDown(self):
        ProductAPITestCase.tearDown(self)


class TestValidProductUpdate(ProductAPITestCase):
    def setUp(self):
        ProductAPITestCase.setUp(self)

        self.query_results = Product.query.filter_by(name=self.product_info['name'])

        self.product_id = self.query_results.first().uuid

    def test_only_quantity_related_to_updating_device_is_updated(self):
        self.skipTest("Cutting corners on time budget")

    def test_only_quantity_related_to_updating_user_is_updated(self):
        self.skipTest("Cutting corners on time budget")

    def test_new_quantity_object_is_inserted_if_first_update(self):
        self.skipTest("Cutting corners on time budget")

    def test_updated_stock_field_in_repsonse_contains_current_stock(self):
        r1 = self.send_product_update(self.product_id, 1, self.dummy_device_id, self.dummy_user_info).json
        r2 = self.send_product_update(self.product_id, 2, self.dummy_device_id, self.dummy_user_info).json
        self.assertEqual(r1.get("updated_stock"), 1)
        self.assertEqual(r2.get("updated_stock"), 3)

    @parameterized.expand([("when_delta_positive", 1),
                           ("when_delta_negative", -1)])
    def test_product_quantity_is_updated(self, _, delta):
        response = self.send_product_update(self.product_id, delta, self.dummy_device_id, self.dummy_user_info)
        self.assertEqual(response.status_code, 202)
        self.assertEqual(response.json['updated_stock'], delta)

    def test_417_if_device_id_not_in_request_json(self):
        response = self.register_new_device(self.dummy_device_id, self.dummy_user.uuid, self.dummy_user_info,
                                            {"delta": 1})
        self.assertEqual(response.status_code, 417)

    def test_quantity_overwrite_logic(self):
        this_device_qty_query = db.session.query(label('quantity', Quantity.quantity), ClientInstance.device_id, UserProduct.product_id) \
            .join(UserProduct)\
            .outerjoin(ClientInstance)\
            .filter(UserProduct.product_id == self.product_id,
                    ClientInstance.device_id == self.dummy_device_id,
                    UserProduct.product_id == self.product_id)

        product_api_logic.update_quantity(self.product_id, self.dummy_device_id, 8, False, user_id=self.dummy_user.uuid)
        first_update_result = this_device_qty_query.all()
        self.assertEqual(len(first_update_result), 1)
        self.assertEqual(first_update_result[0].quantity, 8)

        product_api_logic.update_quantity(self.product_id, self.dummy_device_id, 10, True, user_id=self.dummy_user.uuid)
        second_update_result = this_device_qty_query.all()
        self.assertEqual(len(second_update_result), 1)
        self.assertEqual(second_update_result[0].quantity, 10)

    def tearDown(self):
        ProductAPITestCase.tearDown(self)


class TestValidProductRemoval(ProductAPITestCase, ProductRequestHelpers):
    def setUp(self):
        ProductAPITestCase.setUp(self)
        self.query_results = Product.query.filter_by(name=self.product_info['name'])
        self.original_product_id = self.query_results.first().uuid

        self.device_id = "ANDROID-DEADBEEF"
        self.register_new_device(self.device_id, self.dummy_user.uuid, self.dummy_user_info)
        self.send_product_update(self.original_product_id, 1, self.device_id, self.dummy_user_info)

        quantity_query = db.session.query(Quantity, ClientInstance).filter(ClientInstance.device_id == self.device_id)
        self.assertEqual(quantity_query.count(), 1)
        self.assertEqual(quantity_query.first()[0].quantity, 1)

        self.response = self.send_remove_product_request(self.original_product_id, self.dummy_user_info)

    def test_product_is_not_gone_from_database(self):
        self.assertIsNotNone(Product.query.get(self.original_product_id))

        # product_mappings = UserProduct.query.filter_by(product_id=self.original_product_id,
        #                                                user_id=self.dummy_user.uuid)
        # self.assertEqual(product_mappings.count(), 0)

    def test_product_is_marked_as_removed(self):
        self.assertTrue(Product.query.get(self.original_product_id).is_removed)

    def test_quantity_info_is_not_removed(self):
        quantity_query = db.session.query(Quantity, ClientInstance).filter(ClientInstance.device_id == self.device_id)
        self.assertNotEqual(quantity_query.count(), 0)

    def test_server_repsonse(self):
        self.assertEqual(self.response.status_code, 200)

    def tearDown(self):
        ProductAPITestCase.tearDown(self)
