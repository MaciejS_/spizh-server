import copy

from flask import json
from sqlalchemy import func
from sqlalchemy.sql import label

from spizh.db.model.product import Product
from spizh.db.model.quantity import Quantity
from spizh.db.model.user_product import UserProduct
from spizh.extensions import db


class DeviceRequestHelpers:
    def register_new_device(self, device_id: str, user_id: int, user_credentials: dict, custom_json=None):
        url_template = self.api_version.url_prefix + '/user/{user_id}/device'
        tested_action_uri = url_template.format(user_id=user_id)
        request_json = {"device_id": device_id} if custom_json is None else custom_json
        request_params = {"method": "POST",
                          "path": tested_action_uri,
                          "content_type": "application/json",
                          "data": json.dumps(request_json)}

        return self.send_authorized_request(request_params, user_credentials)

    def get_device_info(self, device_id: str, user_id, user_credentials: dict, fail_on_404=False):
        action_url = '/user/{user_id}/device/{device_id}'.format(user_id=user_id,
                                                                 device_id=device_id)
        request_params = {"method": "GET",
                          "path": self.api_version.url_prefix + action_url}
        return self.send_authorized_request(request_params, user_credentials, fail_on_404)


class ProductRequestHelpers:
    def send_product_insert_request(self, product_info: dict, device_id: str, user_credentials: dict):
        request_data = copy.deepcopy(product_info)
        request_data.update({"device_id": device_id})
        self.product_add_request_params = {"method": "POST",
                                           "path": self.api_version.url_prefix + '/product',
                                           "data": json.dumps(request_data),
                                           "content_type": "application/json"}

        self.response = self.send_authorized_request(self.product_add_request_params, user_credentials)
        product_info.update(self.response.json)
        return product_info

    def send_product_update(self, product_id: int, delta: int, device_id: str, user_credentials: dict,
                            custom_json=None):
        tested_action_uri = self.api_version.url_prefix + '/product/%s/update' % product_id
        if custom_json is None:
            request_params = {"method": "POST",
                              "path": tested_action_uri,
                              "content_type": "application/json",
                              "data": json.dumps({"delta": delta, "device_id": device_id})}
        else:
            request_params = custom_json
        return self.send_authorized_request(request_params, user_credentials)

    def get_all_products_request(self, user_credentials):
        tested_action_uri = self.api_version.url_prefix + '/product'
        request_params = {"method": "GET",
                          "path": tested_action_uri}

        return self.send_authorized_request(request_params, user_credentials)

    def send_remove_product_request(self, product_id, user_credentials):
        self.tested_action_uri = self.api_version.url_prefix + '/product/%s' % product_id
        self.request_params = {"method": "DELETE",
                               "path": self.tested_action_uri,
                               "content_type": "application/json"}
        return self.send_authorized_request(self.request_params, user_credentials)


class UserRequestHelper:
    def send_create_user_request(self, user_info: dict):
        self.request_params = {"method": "POST",
                               "path": self.api_version.url_prefix + '/user',
                               "data": json.dumps(user_info),
                               "content_type": "application/json"}
        return self.send_request(self.request_params)

    def send_get_user_info_request(self, user_id, user_credentials: dict):
        self.tested_action_uri = self.api_version.url_prefix + '/user/%s' % user_id
        self.request_params = {"method": "GET",
                               "path": self.tested_action_uri,
                               "content_type": "application/json"}
        return self.send_authorized_request(self.request_params, user_credentials)


class DBHelpers:
    def get_product_quantity(self, product_id):
        return db.session.query(label('quantity', func.sum(Quantity.quantity)))\
            .join(UserProduct)\
            .join(Product)\
            .filter(Product.uuid == product_id)\
            .group_by(Product.uuid)
