from flask import jsonify
from tst.api.common.commons import ApiTestCase, ProtectedApiTestCase

from spizh.db.model.user import User
from tst.api.common.api_helpers import UserRequestHelper


class TestUserCreation(ApiTestCase, UserRequestHelper):
    def setUp(self):
        ApiTestCase.setUp(self)

        self.user_info = {"name": "Jarema", "auth": "wisnia"}
        self.expected_user = User(**self.user_info)

        self.assertIsNone(User.query.filter_by(name=self.user_info['name']).first())

        self.response = self.send_create_user_request(self.user_info)

        self.query_results = User.query.filter_by(name=self.user_info['name'])

    def test_user_was_actually_inserted(self):
        self.assertIsNotNone(self.query_results)
        self.assertEqual(len(self.query_results.all()), 1)

        actual_user = self.query_results.first()
        self.assertEqual(self.response.json['userid'], actual_user.uuid)

    def test_server_response_is_correct(self):
        actual_user = self.query_results.first()
        expected_response = jsonify({"userid": actual_user.uuid})
        expected_response.status_code = 200

        self.assertEqual(self.response.status_code, expected_response.status_code)

    def tearDown(self):
        ApiTestCase.tearDown(self)


class TestGetUserInfo(ProtectedApiTestCase, UserRequestHelper):
    def setUp(self):
        ApiTestCase.setUp(self)

        self.user_info = self.dummy_user_info
        self.expected_user = self.dummy_user

    def test_server_response_is_correct(self):
        with self.app.test_request_context():
            self.response = self.send_get_user_info_request(self.dummy_user.uuid, self.dummy_user_info)

        expected_response = jsonify({"userid": self.dummy_user.uuid})
        expected_response.status_code = 200

        self.assertEqual(self.response.status_code, expected_response.status_code)

    def test_server_rejects_fake_user(self):
        fake_user_credentials = {"name": "fake", "auth": "user"}
        self.response = self.send_get_user_info_request(self.dummy_user.uuid, fake_user_credentials)

        self.assertEqual(self.response.status_code, 401)

    def tearDown(self):
        ApiTestCase.tearDown(self)
