import unittest

import sqlalchemy
from sqlalchemy.exc import IntegrityError

import spizh.server as spizh
from spizh.db.model.clientinstance import ClientInstance
from tst.server_test import ServerTestCase


class PKTest(ServerTestCase):
    def setUp(self):
        super().setUp()

    def test_raises_when_two_identical_entities_inserted(self):
        device_id = "DEVICE_ID"
        user_id = "USER_ID"
        client_instance = ClientInstance(device_id, user_id)

        spizh.db.session.add(client_instance)
        spizh.db.session.commit()
        inserted_client_instance = ClientInstance.query.filter_by(device_id=device_id, user_id=user_id).first()
        self.assertIsNotNone(inserted_client_instance)

        client_instance_duplicate = ClientInstance(device_id, user_id)

        with self.assertRaises(IntegrityError):
            spizh.db.session.add(client_instance_duplicate)
            spizh.db.session.commit()


    def tearDown(self):
        super().tearDown()


if __name__ == '__main__':
    unittest.main()
