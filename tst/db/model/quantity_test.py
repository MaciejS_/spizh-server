import unittest

import sqlalchemy

import spizh.server as spizh
from spizh.db.model.clientinstance import ClientInstance
from spizh.db.model.product import Product
from spizh.db.model.quantity import Quantity
from spizh.db.model.user import User
from spizh.db.model.user_product import UserProduct
from tst.server_test import ServerTestCase


class QuantityTest(ServerTestCase):
    def setUp(self):
        super().setUp()

        self.user = User("foo", "bar")
        spizh.db.session.add(self.user)
        spizh.db.session.commit()

        self.products = [Product("chleb", 1.20, "piekarnia"), Product("ser", 2.40, "sklep")]
        spizh.db.session.add_all(self.products)
        spizh.db.session.commit()

        self.user_products = [UserProduct(self.user.uuid, product.uuid) for product in self.products]
        spizh.db.session.add_all(self.user_products)
        spizh.db.session.commit()

        self.device_ids = ["ANDROID-C0FFEE", "ANDROID-DEADBEEF"]
        self.client_instances = [ClientInstance(device_id, self.user.uuid) for device_id in self.device_ids]
        spizh.db.session.add_all(self.client_instances)
        spizh.db.session.commit()

        self.quantities = {}
        for user_product in self.user_products:
            for client_instance in self.client_instances:
                self.quantities[(user_product, client_instance)] = Quantity(user_product.uuid, client_instance.uuid, quantity=0)
        spizh.db.session.add_all(self.quantities.values())
        spizh.db.session.commit()

    def test_quantities_deletion_cascades_when_client_instance_removed(self):
        removed_client_instance = self.client_instances[0]
        removed = [q for k, q in self.quantities.items() if k[1] == removed_client_instance]
        retained = [q for k, q in self.quantities.items() if k[1] != removed_client_instance]

        self.assertTrue(all(Quantity.query.get(qty.uuid) for qty in self.quantities.values()),
                        msg="Fixture setup error: Quantity should exist but it does not")

        spizh.db.session.delete(removed_client_instance)
        spizh.db.session.commit()

        self.assertIsNone(ClientInstance.query.get(removed_client_instance.uuid))
        self.assertFalse(any(Quantity.query.get(qty.uuid) for qty in removed),
                             msg="These quantity objects should be deleted by cascade")
        self.assertTrue(all(Quantity.query.get(qty.uuid) for qty in retained),
                             msg="These quantity objects should not be deleted by cascade")

    def test_quantities_deletion_cascades_when_user_product_removed(self):
        removed_user_product = self.user_products[0]
        removed = [q for k, q in self.quantities.items() if k[0] == removed_user_product]
        retained = [q for k, q in self.quantities.items() if k[0] != removed_user_product]

        self.assertTrue(all(Quantity.query.get(qty.uuid) for qty in self.quantities.values()),
                        msg="Fixture setup error: Quantity should exist but it does not")

        spizh.db.session.delete(removed_user_product)
        spizh.db.session.commit()

        self.assertIsNone(UserProduct.query.get(removed_user_product.uuid))

        self.assertFalse(any(Quantity.query.get(qty.uuid) for qty in removed),
                             msg="Quantities of removed UserProduct should aslo be removed")
        self.assertTrue(all(Quantity.query.get(qty.uuid) for qty in retained),
                             msg="Quantities of other UserProducts should not be removed")

    def test_product_deletion_cascades_to_quantities(self):
        removed_product = self.products[0]
        removed = [q for k, q in self.quantities.items() if k[0].product_id == removed_product.uuid]
        retained = [q for k, q in self.quantities.items() if k[0].product_id != removed_product.uuid]

        self.assertTrue(all(Quantity.query.get(qty.uuid) for qty in self.quantities.values()),
                        msg="Fixture setup error: Quantity should exist but it does not")

        spizh.db.session.delete(removed_product)
        spizh.db.session.commit()

        self.assertIsNone(Product.query.get(removed_product.uuid))

        self.assertFalse(any(Quantity.query.get(qty.uuid) for qty in removed),
                         msg="Quantities of removed UserProduct should aslo be removed")
        self.assertTrue(all(Quantity.query.get(qty.uuid) for qty in retained),
                        msg="Quantities of other UserProducts should not be removed")

    def tearDown(self):
        super().tearDown()


if __name__ == '__main__':
    unittest.main()
