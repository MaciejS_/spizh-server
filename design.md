/ - entry point

/products               (GET)   - get a list of all products (id, info_
/products/<id>          (GET)   - get product info
/products/<id>          (POST)  -
/products/<id>          (PUT)   - update product info (only provided fields are updated, unknown fields are ignored) (also used for updating quantities)
/products/<id>/remove   (POST)  - remove product from database

/sync                   (GET)   - request incremental update from server
/sync                   (POST)  - push incremental update to server
