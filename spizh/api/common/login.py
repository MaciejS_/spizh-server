from flask import current_app

from spizh.api.utils import get_user_id_from_auth, make_json_response
from spizh.api.v1.auth import check_password
from spizh.db.model.user import User


def common_return_user_id(username, password):
    try:
        if check_password(username, password):
            # TODO: respond with redirect if device is not known?
            user_id = get_user_id_from_auth()
            return make_json_response({"userid": user_id}, 200)
        else:
            if (len(username) > 0):
                user_instance = User.query.filter_by(name=username).first()
                current_app.logger.error((username, password))
                if not user_instance:
                    return '', 404
            return '', 401
    except AttributeError as e:
        current_app.logger.error((username, password))
        return '', 401