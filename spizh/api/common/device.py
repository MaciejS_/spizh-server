from sqlalchemy.exc import IntegrityError

from spizh.api.utils import get_user_id_from_auth, make_json_response
from spizh.db.model.clientinstance import ClientInstance
from spizh.extensions import db


def common_add_device(user_id, json):
    device_id_json_key = 'device_id'
    auth_user_id = get_user_id_from_auth()
    if user_id != auth_user_id:
        return '', 403
    if device_id_json_key not in json:
        return make_json_response({"msg": 'DeviceID is missing from request data'}, 417)
    device_id = json[device_id_json_key]
    client_instance = ClientInstance(device_id, user_id)
    try:
        db.session.add(client_instance)
        db.session.commit()
        return make_json_response({"msg": 'Device added'}, 202)
    except IntegrityError as e:
        # user-device mapping already exists in the database
        return make_json_response({"msg": "Device already registered"}, 409)


def common_get_device_info(device_id, user_id):
    client_instance = ClientInstance.query.filter_by(user_id=user_id,
                                                     device_id=device_id).first()
    if client_instance:
        return '', 200
    else:
        return '', 404