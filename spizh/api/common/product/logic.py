import time
from sqlalchemy.exc import IntegrityError

from spizh.db.model.product_price import ProductPrice
from spizh.api.common.product.exception import NoSuchProductError, ProductRemovalError
from spizh.api.common.sync.exception import InvalidSchemaError
from spizh.api.utils import get_user_id_from_auth
from spizh.db.model.clientinstance import ClientInstance
from spizh.db.model.product import Product
from spizh.db.model.quantity import Quantity
from spizh.db.model.user_product import UserProduct
from spizh.extensions import db


def insert_product(product_info: dict, user_id: int) -> int:
    """
    Inserts a product to the database and returns a ProductID
    If there's a database error, the transaction gets rolled back and error is re-raised

    :param product_info: deserialized json with information about a product
    :param user_id: userID
    :return: productID assigned by the database
    """
    try:
        product = Product(uuid=product_info['uuid'],
                          name=product_info['name'],
                          price=product_info['price'],
                          store=product_info['store'])
        db.session.add(product)
        db.session.flush()

        user_product = UserProduct(user_id, product.uuid)
        db.session.add(user_product)
        db.session.flush()

        # v2 compatibility, product price won't be duplicated thanks to unique constraints
        product_price = ProductPrice(product_id=product_info['uuid'],
                                     price=product_info['price'],
                                     store=product_info['store'],
                                     last_update_timestamp=int(time.time()))
        db.session.add(product_price)
        db.session.commit()
        return product.uuid

    except KeyError as e:
        raise InvalidSchemaError(msg="%s info is missing" % str(e), cause=product_info)
    except IntegrityError as e:
        db.session.rollback()
        raise e


def update_quantity(product_id, device_id, quantity_delta, overwrite=False, user_id=None) -> (dict, int):
    """
    Performs an atomic update to quantity of product with `product_id`, recorded on device with `device_id`
    :param user_id:
    :param product_id: int product id
    :param device_id: str, android_id of a device
    :param quantity_delta: int amount by which the product stock will change
    :param overwrite: bool, if True, existing delta will be overwritten with new data
    :return: tuple (response_json, status_code)
    """
    user_id = user_id or get_user_id_from_auth()
    user_product = UserProduct.query.filter_by(user_id=user_id, product_id=product_id).first()

    if user_product:
        client_instance = ClientInstance.query.filter_by(user_id=user_id, device_id=device_id).first()
        quantity_object = Quantity(user_product_id=user_product.uuid,
                                   client_instance_id=client_instance.uuid)
        try:
            db_quantity_object = Quantity.query.filter_by(user_product_id=quantity_object.user_product_id,
                                                          client_instance_id=quantity_object.client_instance_id).first()
        except IntegrityError as e:
            db_quantity_object = None

        if not db_quantity_object:
            try:
                quantity_object.quantity = quantity_delta
                quantity_object = _insert_new_quantity_object(quantity_object)
            except IntegrityError as e:
                raise
        else:
            quantity_object = db_quantity_object
            _update_existing_quantity_object(quantity_object, quantity_delta, overwrite)
        return quantity_object.quantity
    else:
        raise NoSuchProductError

def _update_existing_quantity_object(quantity, quantity_delta, overwrite):
    # this is not the first update and quantity entity already exists
    if overwrite:
        quantity.quantity = quantity_delta
    else:
        quantity.quantity += quantity_delta
    db.session.commit()


def _insert_new_quantity_object(quantity_object) -> Quantity:
    """
    Inserts a new Quantity object into the database and returns its data
    :param quantity_object:
    :return: quantity_object updated with uuid
    """
    db.session.add(quantity_object)
    db.session.commit()
    return quantity_object


def remove_product(product_id):
    product = Product.query.get(product_id)
    try:
        product.mark_as_removed()
        db.session.commit()
    except IntegrityError as e:
        db.session.rollback()
        raise ProductRemovalError


def get_all_products(user_id: int) -> dict:
    """
    Returns a list of all Products for given `user_id` as dicts, complete with quantities
    :param user_id: int, ID of
    :return: dict of id:product_info_dict pairs
    """
    all_products = {}
    products = UserProduct.query \
        .filter_by(user_id=user_id) \
        .join(Product) \
        .add_columns(UserProduct.uuid, Product.name, Product.price, Product.store) \
        .all()
    for product in products:
        # TODO: colud be optimized by doing the aggregation inside the query
        quantities = Quantity.query.filter_by(user_product_id=product.uuid).all()
        id = product.UserProduct.product_id
        all_products[id] = {"name": product.name,
                            "quantity": sum((qty.quantity for qty in quantities), 0),
                            "price": float(product.price),
                            "store": product.store}

    return all_products
