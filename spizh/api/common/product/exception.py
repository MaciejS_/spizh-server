class NoSuchProductError(Exception):
    pass


class ProductRemovalError(object):
    pass


class ProductInsertionError(Exception):
    def __init__(self, msg, cause, *args) -> None:
        super().__init__(*args)
        self._message = msg
        self._cause = cause

    @property
    def message(self):
        return self._message

    @property
    def cause(self):
        return self._cause


class QuantityUpdateError(Exception):
    def __init__(self, msg, cause, *args) -> None:
        super().__init__(*args)
        self._message = msg
        self._cause = cause

    @property
    def message(self):
        return self._message

    @property
    def cause(self):
        return self._cause
