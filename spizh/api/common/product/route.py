from flask import jsonify

from spizh.api.common.product.logic import get_all_products


def common_get_all_products(user_id):
    response = jsonify(get_all_products(user_id))
    response.status_code = 200
    return response