from spizh.db.model.user import User


def check_password(password, username):
    # TODO: do not store unhashed passwords
    user = User.query.filter_by(name=username).first()
    if user:
        return user.auth == password
    else:
        return False
