from sqlalchemy import func
from sqlalchemy.sql import label

from spizh.db.model.clientinstance import ClientInstance
from spizh.db.model.product import Product
from spizh.db.model.quantity import Quantity
from spizh.db.model.user_product import UserProduct
from spizh.extensions import db


def query_quantity_of_products_not_known_by_client_instance(user_id, device_id) -> db.Query:
    return db.session.query(Quantity.quantity, label('product_id', Product.uuid), ClientInstance.device_id) \
        .join(UserProduct) \
        .join(ClientInstance) \
        .join(Product)\
        .filter(ClientInstance.device_id != device_id,
                ClientInstance.user_id == user_id).subquery('remote_products')


def query_aggregate_remote_quantity_updates(device_id, user_id):
    """
    Returns Query object with two columns: "product_id" and "delta_sum", containing aggregated remote deltas for all products tracked by `user_id`
    :param device_id: id
    :param user_id: id of user whose products will be queried
    :return:
    """
    remote_products = query_quantity_of_products_not_known_by_client_instance(user_id, device_id)
    quantity_updates = db.session.query(label("product_id", remote_products.c.product_id),
                                        label('delta_sum', func.sum(remote_products.c.quantity))) \
        .group_by(remote_products.c.product_id)
    return quantity_updates


def query_remote_products(device_id, user_id):
    remote_quantities = query_quantity_of_products_not_known_by_client_instance(user_id, device_id)
    new_products = db.session.query(Product)\
        .outerjoin(remote_quantities, remote_quantities.c.product_id == Product.uuid)\
        .filter(remote_quantities.c.device_id != device_id)
    return new_products

def query_removed_products(user_id):
    removed_products = db.session.query(Product)\
        .outerjoin(UserProduct) \
        .filter(Product.is_removed == True,
                UserProduct.user_id == user_id)

    return removed_products