from spizh.db.model.product import Product


def matches_product_update_schema(product_update: dict) -> bool:
    """
    Returns True if product update block matches expected schema
    :param product_update: product update block
    :return: bool
    """
    return all(required_key in product_update for required_key in ["uuid", "name", "price", "store"])


def matches_quantity_update_schema(quantity_update: dict):
    return all(required_key in quantity_update for required_key in ["product_id", "delta"])


def product_already_exists(product: Product):
    return Product.query.filter_by(**product.to_dict()).first() is not None