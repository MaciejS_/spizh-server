def assemble_product_updates_bundle(products: list) -> dict:
    """
    Returns a list of dicts that contain basic product info (uuid, name, store and price)
    :param products:
    :return:
    """
    # outgoing_product_updates = {}
    outgoing_product_updates = []
    for new_product in products:
        product_info_dict = {
            "uuid": new_product.uuid,
            "name": new_product.name,
            "store": new_product.store,
            "price": float(new_product.price)}
        outgoing_product_updates.append(product_info_dict)
        # outgoing_product_updates[new_product.uuid] = product_info_dict
    return outgoing_product_updates


def assemble_quantity_updates_bundle(quantity_updates: list) -> dict:
    """
    Returns a dict of (product_id : product_quantity) pairs
    :param quantity_updates: result og aggregate
    :return:
    """
    quantity_updates_bundle = {quantity.product_id: quantity.delta_sum for quantity in quantity_updates}
    return quantity_updates_bundle


def assemble_outgoing_update(product_updates, quantity_updates, to_remove):
    updates_json = {}
    if product_updates or to_remove:
        updates_json["product"] = {}
        if product_updates is not None:
            updates_json["product"]["added"] = product_updates
        if to_remove is not None:
            updates_json["product"]["removed"] = to_remove

    if quantity_updates is not None:
        updates_json["quantity"] = quantity_updates

    return updates_json
