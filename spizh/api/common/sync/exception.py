class InvalidSchemaError(Exception):
    def __init__(self, msg, cause, *args) -> None:
        super().__init__(*args)
        self._message = msg
        self._cause = cause

    @property
    def message(self):
        return self._message

    @property
    def cause(self):
        return self._cause
