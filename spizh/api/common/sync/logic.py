from spizh.api.common.product.logic import insert_product, remove_product, update_quantity
from sqlalchemy.exc import IntegrityError

from spizh.api.common.product.exception import NoSuchProductError, QuantityUpdateError
from spizh.api.common.sync.db import query_aggregate_remote_quantity_updates, query_remote_products, \
    query_removed_products
from spizh.api.common.sync.exception import InvalidSchemaError
from spizh.api.common.sync.transformer import assemble_product_updates_bundle, assemble_quantity_updates_bundle
from spizh.api.common.sync.utils import matches_product_update_schema
from spizh.api.utils import get_user_id_from_auth


def apply_incoming_update(updates_bundle: dict, device_id: str):
    """
    Applies the `update_bundle` received from the remote device
    If there were any updates to products, a map translating device-assigned product ids to server-assigned product ids is returned

    :param updates_bundle: structured update
    :param device_id:
    :return: dict of pairs (device-assigned_pid : server-assigned_pid) or None if there were no new products inserted
    """

    if 'product' in updates_bundle:
        product_updates = updates_bundle['product']
        if 'added' in product_updates:
            insert_all_products(product_updates.get("added", None))
        if 'removed' in product_updates:
            remove_all_listed_products(product_updates.get("removed", None))
            # clientside base will need an 'onupdate' cascade in quantities table
    if 'quantity' in updates_bundle:
        quantity_updates = updates_bundle.get("quantity")
        apply_quantity_updates(quantity_updates, device_id)


def insert_all_products(product_updates: list):
    """
    Validates all product updates and, if there are no errors, adds all of them to the database.
    If any of the updates is erroneous, the procedure is aborted and database isn't updated
    In case of successful update - a list of new ids for inserted products is returned - due to client asynchronicity, those ids might already be taken

    :raise InvalidSchemaError: if either of product updates has malformed schema
    :raise ProductInsertionError: if there's an error on product insert
    :param product_updates: list of serialized product updates - as JSON
    :return:
    """
    for serialized_product in product_updates:
        if not matches_product_update_schema(serialized_product):
            raise InvalidSchemaError(msg="Malformed json schema", cause=serialized_product)

    user_id = get_user_id_from_auth()

    for serialized_product in product_updates:
        # tmp_id = serialized_product["tmp_id"]
        try:
            insert_product(serialized_product, user_id)
        except IntegrityError as e:
            pass  # product already exists
            # raise ProductInsertionError(msg="Product (ID %s) caused an error upon insertion" % serialized_product["uuid"],
            #                             cause=e)


def remove_all_listed_products(removed_products_ids: list):
    """
    Remove each of listed products
    Because removal only involves marking the product as removed, rather than physically removing it, it's safe to remove a product twice

    :param removed_products_ids:
    :return:
    """
    for id in removed_products_ids:
        remove_product(id)


def apply_quantity_updates(quantity_updates: list, device_id: str):
    """
    Applies all quantity updates in `quantity_updates` list

    :raise InvalidSchemaError: if any of update dicts doesn't have required keyr
    :param quantity_updates: list of quantity updates as dicts
    :param device_id:
    :return: None
    """
    # for q in quantity_updates:
    #     if not matches_quantity_update_schema(q):
    #         raise InvalidSchemaError(msg="Malformed json schema", cause=q)

    for uuid, qty in quantity_updates.items():
        try:
            update_quantity(uuid, device_id, qty, overwrite=True)
        except NoSuchProductError as exc:
            # "Attempt to update quantity for a product that doesn't exist"
            raise exc
        except QuantityUpdateError as exc:
            raise RuntimeError("Failure during quantity update - we're not prepared to handle that yet")


def get_product_updates(user_id, device_id):
    new_products = query_remote_products(device_id, user_id).all()
    return assemble_product_updates_bundle(new_products)


def get_quantity_updates(user_id, device_id):
    quantity_updates = query_aggregate_remote_quantity_updates(device_id, user_id)

    return assemble_quantity_updates_bundle(quantity_updates)


def get_products_to_remove(user_id):
    return [product.uuid for product in query_removed_products(user_id).all()]
