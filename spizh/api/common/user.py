import re

from sqlalchemy.exc import IntegrityError

from spizh.api.error import NoSuchUserException
from spizh.api.error import UserAlreadyExistsException
from spizh.api.utils import make_json_response
from spizh.db.model.user import User
from spizh.extensions import db


def add_user(username, password):
    # TODO: Missing string sanitization
    new_user = User(name=username, auth=password)

    try:
        db.session.add(new_user)
        db.session.commit()

        response = make_json_response({"userid": new_user.uuid}, 200)
        return response

    except IntegrityError as e:
        db.session.rollback()
        if re.match("UNIQUE constraint failed", e.orig):
            raise UserAlreadyExistsException(message="User {username} already exists".format(new_user.name),
                                             payload=new_user)
        else:
            raise e


def common_get_user_info(user_id):
    user = User.query.get(user_id)
    if not user:
        raise NoSuchUserException(user)
    return make_json_response(user.to_dict(), 200)
