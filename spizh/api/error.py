from spizh.db.model.user import User


class UserAlreadyExistsException(Exception):
    status_code = 409

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


class NoSuchUserException(Exception):
    status_code = 404

    def __init__(self, user: User):
        Exception.__init__(self)
        self.message = "User with id {id} doesn't exist!".format(id=user.uuid)
