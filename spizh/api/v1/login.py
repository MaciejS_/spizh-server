from flask import request

from spizh.api.common.login import common_return_user_id
from spizh.api.v1 import api_v1


@api_v1.route('/login', methods=['GET'])
def return_user_id():
    return common_return_user_id(request.authorization.username, request.authorization.password)
