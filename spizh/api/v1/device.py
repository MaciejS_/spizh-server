from flask import request

from spizh.api.common.device import common_add_device, common_get_device_info
from spizh.api.v1 import api_v1, api_v1_basic_auth


# TODO: sending user device_id in GET (the ACTUAL one, not our internal ID) is not a good solution security-wise
@api_v1.route('/user/<user_id>/device/<device_id>', methods=['GET'])
@api_v1_basic_auth.login_required
def get_device_info(user_id: int, device_id: str):
    return common_get_device_info(device_id, user_id)


@api_v1.route('/user/<user_id>/device', methods=['POST'])
@api_v1_basic_auth.login_required
def add_device(user_id: str):
    return common_add_device(user_id, request.get_json())
