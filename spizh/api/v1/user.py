from flask import request

from spizh.api.common.user import common_get_user_info
from spizh.api.common.user import add_user
from spizh.api.v1 import api_v1, api_v1_basic_auth


@api_v1.route('/user', methods=['POST'])
def create_user():
    request_json = request.get_json()
    return add_user(username=request_json['name'],
                    password=request_json['auth'])


@api_v1.route('/user/<user_id>', methods=['GET'])
@api_v1_basic_auth.login_required
def get_user_info(user_id):
    return common_get_user_info(user_id)
