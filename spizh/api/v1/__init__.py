import flask
from flask_httpauth import HTTPBasicAuth

api_v1 = flask.Blueprint("api_v1", __name__, url_prefix='/api/v1.0')
api_v1_basic_auth = HTTPBasicAuth()

import spizh.api.v1.auth
import spizh.api.v1.error

import spizh.api.v1.device
import spizh.api.v1.login
import spizh.api.v1.product
import spizh.api.v1.sync
import spizh.api.v1.user
