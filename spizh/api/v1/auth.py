from spizh.api.v1 import api_v1_basic_auth
from spizh.api.common.auth import check_password as common_check_password


@api_v1_basic_auth.verify_password
def check_password(username, password):
    return common_check_password(password, username)
