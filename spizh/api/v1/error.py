from flask import jsonify

from spizh.api.error import UserAlreadyExistsException, NoSuchUserException
from spizh.api.v1 import api_v1


@api_v1.errorhandler(UserAlreadyExistsException)
def handle_user_already_exists_error(error):
    response = jsonify({"error": error.message})
    response.status_code = error.status
    return response


@api_v1.errorhandler(NoSuchUserException)
def handle_user_already_exists_error(error):
    response = jsonify({"error": error.message})
    response.status_code = error.status
    return response
