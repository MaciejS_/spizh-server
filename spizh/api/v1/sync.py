from flask import request, jsonify

from spizh.api.common.product.exception import ProductInsertionError, NoSuchProductError
from spizh.api.common.sync.exception import InvalidSchemaError
from spizh.api.common.sync.logic import apply_incoming_update, get_product_updates, get_quantity_updates, \
    get_products_to_remove
from spizh.api.common.sync.transformer import assemble_outgoing_update
from spizh.api.utils import get_user_id_from_auth
from spizh.api.v1 import api_v1
from spizh.api.v1 import api_v1_basic_auth
from spizh.db.model.clientinstance import ClientInstance


@api_v1.route('/sync', methods=['GET'])
@api_v1_basic_auth.login_required
def get_last_sync_info():
    json = request.get_json()
    device_id = json['device_id']
    # this won't be required
    # returns current server-side client clock


@api_v1.route('/sync', methods=['POST'])
@api_v1_basic_auth.login_required
def exchange_update_batches():
    json = request.get_json()
    device_id = json.get("device_id")

    if not device_id:
        return 'Device ID missing', 400
    if not ClientInstance.query.filter_by(device_id=device_id).first():
        return 'Unregistered device', 412

    user_id = get_user_id_from_auth()

    try:
        apply_incoming_update(json, device_id)
    except InvalidSchemaError as exc:
        return 'Malformed JSON schema in object: %s' % str(exc.cause), 406
    except NoSuchProductError as exc:
        return "Attempt to update a product that doesn't exist", 406
    except ProductInsertionError as exc:
        return exc.message, 500

    outgoing_product_updates = get_product_updates(user_id, device_id)
    outgoing_quantity_updates = get_quantity_updates(user_id, device_id)
    outgoing_products_to_remove = get_products_to_remove(user_id)
    outgoing_update_json = assemble_outgoing_update(outgoing_product_updates,
                                                    outgoing_quantity_updates,
                                                    outgoing_products_to_remove)
    response_json = outgoing_update_json

    returned_request = jsonify(response_json)
    returned_request.status_code = 202
    return returned_request

