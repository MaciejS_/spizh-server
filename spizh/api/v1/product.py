from flask import request
from sqlalchemy.exc import IntegrityError

from spizh.api.common.product.route import common_get_all_products
from spizh.api.common.product.exception import NoSuchProductError, ProductRemovalError
from spizh.api.common.product.logic import insert_product, remove_product, update_quantity
from spizh.api.utils import get_user_id_from_auth, make_json_response
from spizh.api.v1 import api_v1
from spizh.api.v1 import api_v1_basic_auth


@api_v1.route('/product', methods=['POST'])
@api_v1_basic_auth.login_required
def add_product_action():
    request_json = request.get_json()
    user_id = get_user_id_from_auth()
    product_info = {key: request_json[key] for key in ["price", "name", "store", "uuid"]}

    try:
        product_id = insert_product(product_info, user_id)
        response_body = {"id": product_id}
        response = make_json_response(response_body, 201)
        return response
    except IntegrityError as e:
        raise e


@api_v1.route('/product', methods=['GET'])
@api_v1_basic_auth.login_required
def get_all_products_action():
    user_id = get_user_id_from_auth()

    return common_get_all_products(user_id)


@api_v1.route('/product/<product_id>/update', methods=['POST'])
@api_v1_basic_auth.login_required
def update_product_stock_action(product_id):
    request_json = request.get_json()
    delta = request_json['delta']
    if 'device_id' not in request_json:
        return 'device_id not provided!', 417
    device_id = request_json['device_id']

    try:
        new_quantity = update_quantity(product_id, device_id, delta)
        return make_json_response({'updated_stock': new_quantity}, 202)
    except NoSuchProductError as e:
        return make_json_response({"error": "No such product"}, status_code=404)


@api_v1.route('/product/<product_id>', methods=['DELETE'])
@api_v1_basic_auth.login_required
def remove_product_action(product_id):
    try:
        remove_product(product_id)
    except ProductRemovalError as e:
        return 'Server error occurred when removing product', 500
    return '', 200


