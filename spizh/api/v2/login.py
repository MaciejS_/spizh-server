from flask import request

from spizh.api.common.login import common_return_user_id
from spizh.api.v2 import api_v2


@api_v2.route('/login', methods=['GET'])
def return_user_id():
    return common_return_user_id(request.authorization.username, request.authorization.password)
