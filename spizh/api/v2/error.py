from flask import jsonify

from spizh.api.error import UserAlreadyExistsException, NoSuchUserException
from spizh.api.v2 import api_v2


@api_v2.errorhandler(UserAlreadyExistsException)
def handle_user_already_exists_error(error):
    response = jsonify({"error": error.message})
    response.status_code = error.status
    return response


@api_v2.errorhandler(NoSuchUserException)
def handle_user_already_exists_error(error):
    response = jsonify({"error": error.message})
    response.status_code = error.status
    return response
