from spizh.db.model.user_product import UserProduct
from spizh.db.model.product import Product
from spizh.db.model.product_price import ProductPrice
from spizh.extensions import db


def query_updated_products(user_id):
    updated_products = db.session.query(Product)\
        .outerjoin(UserProduct) \
        .filter(Product.is_removed == False,
                UserProduct.user_id == user_id)

    return updated_products

def query_price_updates(user_id):
    prices = db.session.query(ProductPrice)\
        .outerjoin(Product)\
        .outerjoin(UserProduct)\
        .filter(UserProduct.user_id == user_id)

    return prices