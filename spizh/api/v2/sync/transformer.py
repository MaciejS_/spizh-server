from spizh.api.common.sync.transformer import assemble_outgoing_update as assemble_v1_outgoing_update

def assemble_product_updates_bundle(products: list) -> dict:
    """
    Returns a list of dicts that contain basic product info (uuid, name, store and price)
    :param products:
    :return:
    """
    # outgoing_product_updates = {}
    outgoing_product_updates = []
    for new_product in products:
        product_info_dict = {
            "uuid": new_product.uuid,
            "name": new_product.name,
            "store": new_product.store,
            "price": float(new_product.price),
            "priority": int(new_product.priority),
            "last_update_timestamp": int(new_product.last_update_timestamp)}
        outgoing_product_updates.append(product_info_dict)
        # outgoing_product_updates[new_product.uuid] = product_info_dict
    return outgoing_product_updates


def assemble_price_updates_bundle(price_updates):
    outgoing_price_updates = []
    for price in price_updates:
        price_info_dict = {
            "uuid": price.uuid,
            "user_product_uuid": price.product_id,
            "store": price.store,
            "price": float(price.price),
            "last_update_timestamp": price.last_update_timestamp,
            "is_removed": price.is_removed}
        outgoing_price_updates.append(price_info_dict)
    return outgoing_price_updates


def assemble_outgoing_update(product_updates, quantity_updates, to_remove, property_updates, price_updates) -> dict:
    v1_updates_bundle = assemble_v1_outgoing_update(product_updates=product_updates,
                                                    quantity_updates=quantity_updates,
                                                    to_remove=to_remove)
    if property_updates:
        if "product" not in v1_updates_bundle:
            v1_updates_bundle["product"] = {}
        v1_updates_bundle["product"]["updated"] = property_updates

    if price_updates:
        v1_updates_bundle["price"] = price_updates

    return v1_updates_bundle