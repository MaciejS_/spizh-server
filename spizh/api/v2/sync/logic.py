from sqlalchemy.exc import IntegrityError

from spizh.api.common.product.exception import NoSuchProductError, QuantityUpdateError
from spizh.api.common.product.logic import remove_product, update_quantity
from spizh.api.common.sync.db import query_aggregate_remote_quantity_updates, query_remote_products, \
    query_removed_products
from spizh.api.common.sync.exception import InvalidSchemaError
from spizh.api.common.sync.transformer import assemble_quantity_updates_bundle
from spizh.api.utils import get_user_id_from_auth
from spizh.api.v2.product.logic import insert_product, update_product
from spizh.api.v2.sync.db import query_updated_products, query_price_updates
from spizh.api.v2.sync.transformer import assemble_product_updates_bundle, assemble_price_updates_bundle
from spizh.api.v2.sync.utils import matches_product_update_schema
from spizh.db.model.product_price import ProductPrice
from spizh.extensions import db


def apply_incoming_update(updates_bundle: dict, device_id: str):
    """
    Applies the `update_bundle` received from the remote device
    If there were any updates to products, a map translating device-assigned product ids to server-assigned product ids is returned

    :param updates_bundle: structured update
    :param device_id:
    :return: dict of pairs (device-assigned_pid : server-assigned_pid) or None if there were no new products inserted
    """

    if 'product' in updates_bundle:
        product_updates = updates_bundle['product']
        if 'added' in product_updates:
            insert_all_products(product_updates.get("added", None))
        if 'updated' in product_updates:
            update_all_products(product_updates.get("updated", None))
        if 'removed' in product_updates:
            remove_all_listed_products(product_updates.get("removed", None))
            # clientside base will need an 'onupdate' cascade in quantities table
    if 'quantity' in updates_bundle:
        quantity_updates = updates_bundle.get("quantity")
        apply_quantity_updates(quantity_updates, device_id)
    if 'price' in updates_bundle:
        price_updates = updates_bundle.get("price")
        apply_price_updates(price_updates)


def insert_all_products(product_updates: list):
    """
    Validates all product updates and, if there are no errors, adds all of them to the database.
    If any of the updates is erroneous, the procedure is aborted and database isn't updated
    In case of successful update - a list of new ids for inserted products is returned - due to client asynchronicity, those ids might already be taken

    :raise InvalidSchemaError: if either of product updates has malformed schema
    :raise ProductInsertionError: if there's an error on product insert
    :param product_updates: list of serialized product updates - as JSON
    :return:
    """
    for serialized_product in product_updates:
        if not matches_product_update_schema(serialized_product):
            raise InvalidSchemaError(msg="Malformed json schema", cause=serialized_product)

    user_id = get_user_id_from_auth()

    for serialized_product in product_updates:
        # tmp_id = serialized_product["tmp_id"]
        try:
            insert_product(serialized_product, user_id)
        except IntegrityError as e:
            pass  # product already exists
            # raise ProductInsertionError(msg="Product (ID %s) caused an error upon insertion" % serialized_product["uuid"],
            #                             cause=e)


def update_all_products(product_updates: list):
    for serialized_product in product_updates:
        try:
            update_product(serialized_product)
        except IntegrityError as e:
            raise e  # that shouldn't happen - product update shouldn't cause any sync problems


def remove_all_listed_products(removed_products_ids: list):
    """
    Remove each of listed products
    Because removal only involves marking the product as removed, rather than physically removing it, it's safe to remove a product twice

    :param removed_products_ids:
    :return:
    """
    for id in removed_products_ids:
        remove_product(id)


def apply_quantity_updates(quantity_updates: list, device_id: str):
    """
    Applies all quantity updates in `quantity_updates` list

    :raise InvalidSchemaError: if any of update dicts doesn't have required keyr
    :param quantity_updates: list of quantity updates as dicts
    :param device_id:
    :return: None
    """
    # for q in quantity_updates:
    #     if not matches_quantity_update_schema(q):
    #         raise InvalidSchemaError(msg="Malformed json schema", cause=q)

    for uuid, qty in quantity_updates.items():
        try:
            update_quantity(uuid, device_id, qty, overwrite=True)
        except NoSuchProductError as exc:
            # "Attempt to update quantity for a product that doesn't exist"
            raise exc
        except QuantityUpdateError as exc:
            raise RuntimeError("Failure during quantity update - we're not prepared to handle that yet")


def get_product_updates(user_id, device_id):
    new_products = query_remote_products(device_id, user_id).all()
    return assemble_product_updates_bundle(new_products)


def get_product_property_updates(user_id):
    products_to_update = query_updated_products(user_id)
    return assemble_product_updates_bundle(products_to_update)


def get_price_updates(user_id):
    price_updates = query_price_updates(user_id)
    return assemble_price_updates_bundle(price_updates)


def get_quantity_updates(user_id, device_id):
    quantity_updates = query_aggregate_remote_quantity_updates(device_id, user_id)
    return assemble_quantity_updates_bundle(quantity_updates)


def get_products_to_remove(user_id):
    return [product.uuid for product in query_removed_products(user_id).all()]


def apply_price_updates(price_updates):
    for price_json in price_updates:
        try:
            price = ProductPrice(uuid=price_json['uuid'],
                                   product_id=price_json['user_product_uuid'],
                                   price=price_json['price'],
                                   store=price_json['store'],
                                   last_update_timestamp=price_json['last_update_timestamp'],
                                 is_removed=price_json['is_removed'])

            adjust_uuid_after_migration(price)
            existsing_product_price = db.session.query(ProductPrice).filter_by(uuid=price.uuid).first()

            if existsing_product_price:
                if existsing_product_price.last_update_timestamp < price.last_update_timestamp:
                    try:
                        existsing_product_price.price = price.price
                        existsing_product_price.last_update_timestamp = price.last_update_timestamp
                        existsing_product_price.is_removed = price.is_removed
                        db.session.add(existsing_product_price)
                        db.session.commit()
                    except IntegrityError as e:
                        db.session.rollback()
                        raise e  # that should not happen - new price infos should be added without problems

            else:
                try:
                    db.session.add(price)
                    db.session.commit()
                except IntegrityError as e:
                    db.session.rollback()
                    raise e # that should not happen - new price infos should be added without problems

        except KeyError as e:
            raise InvalidSchemaError(msg="%s info is missing" % str(e), cause=price_json)

            # raise e
            # this integrity erro shouldn't be a problem if it is caused by duplicate


def adjust_uuid_after_migration(external_price):
    existsing_product_prices = db.session.query(ProductPrice).filter_by(product_id=external_price.product_id)

    for price in existsing_product_prices:
        if external_price.uuid != price.uuid:
            if price.store == external_price.store:
                price.uuid = external_price.uuid
                db.session.add(price)
                db.session.commit()
