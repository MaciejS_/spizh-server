import flask
from flask_httpauth import HTTPBasicAuth

api_v2 = flask.Blueprint("api_v2", __name__, url_prefix='/api/v2.0')
api_v2_basic_auth = HTTPBasicAuth()

import spizh.api.v2.auth
import spizh.api.v2.error

import spizh.api.v2.device
import spizh.api.v2.login
import spizh.api.v2.product.route
import spizh.api.v2.sync.route
import spizh.api.v2.user
