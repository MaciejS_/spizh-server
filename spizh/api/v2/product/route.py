from flask import request, jsonify
from spizh.api.common.product.logic import remove_product, update_quantity
from spizh.api.v2.product.logic import insert_product, get_all_products, update_priority
from sqlalchemy.exc import IntegrityError

from spizh.api.common.product.exception import NoSuchProductError, ProductRemovalError
from spizh.api.utils import get_user_id_from_auth, make_json_response
from spizh.api.v2 import api_v2
from spizh.api.v2 import api_v2_basic_auth


@api_v2.route('/product', methods=['POST'])
@api_v2_basic_auth.login_required
def add_product_action():
    request_json = request.get_json()
    user_id = get_user_id_from_auth()
    product_info = {key: request_json[key] for key in ["price", "name", "store", "uuid", "priority"]}

    try:
        product_id = insert_product(product_info, user_id)
        response_body = {"id": product_id}
        response = make_json_response(response_body, 201)
        return response
    except IntegrityError as e:
        raise e


@api_v2.route('/product', methods=['GET'])
@api_v2_basic_auth.login_required
def get_all_products_action():
    user_id = get_user_id_from_auth()

    response = jsonify(get_all_products(user_id))
    response.status_code = 200
    return response


@api_v2.route('/product/<product_id>/update', methods=['POST'])
@api_v2_basic_auth.login_required
def update_product_stock_action(product_id):
    request_json = request.get_json()
    if 'device_id' not in request_json:
        return 'device_id not provided!', 417
    device_id = request_json['device_id']

    try:
        if 'delta' in request_json:
            delta = request_json['delta']
            result = update_quantity(product_id, device_id, delta)
        if 'priority' in request_json:
            new_priority = request_json['priority']
            result = update_priority(product_id, new_priority)
        if 'product_info' in request_json:
            raise NotImplementedError

        return make_json_response({'result': result}, 202)
    except NoSuchProductError as e:
        return make_json_response({"error": "No such product"}, status_code=404)

@api_v2.route('/product/<product_id>', methods=['DELETE'])
@api_v2_basic_auth.login_required
def remove_product_action(product_id):
    try:
        remove_product(product_id)
    except ProductRemovalError as e:
        return 'Server error occurred when removing product', 500
    return '', 200


