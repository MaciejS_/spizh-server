import time
from sqlalchemy.exc import IntegrityError

from spizh.db.model.product_price import ProductPrice
from spizh.api.common.product.exception import NoSuchProductError
from spizh.api.common.sync.exception import InvalidSchemaError
from spizh.db.model.product import Product
from spizh.db.model.quantity import Quantity
from spizh.db.model.user_product import UserProduct
from spizh.extensions import db


def insert_product(product_info: dict, user_id: int) -> int:
    """
    Inserts a product to the database and returns a ProductID
    If there's a database error, the transaction gets rolled back and error is re-raised

    :param product_info: deserialized json with information about a product
    :param user_id: userID
    :return: productID assigned by the database
    """
    try:
        product = Product(uuid=product_info['uuid'],
                          name=product_info['name'],
                          price=product_info['price'],
                          store=product_info['store'],
                          priority=product_info['priority'])
        db.session.add(product)
        db.session.flush()

        user_product = UserProduct(user_id, product.uuid)
        db.session.add(user_product)
        db.session.flush()

        product_price = ProductPrice(product_id=product_info['uuid'],
                                     price=product_info['price'],
                                     store=product_info['store'],
                                     last_update_timestamp=int(time.time()))
        db.session.add(product_price)
        db.session.commit()
    except KeyError as e:
        raise InvalidSchemaError(msg="%s info is missing" % str(e), cause=product_info)
    except IntegrityError as e:
        db.session.rollback()
        raise e


def get_all_products(user_id: int) -> dict:
    """
    Returns a list of all Products for given `user_id` as dicts, complete with quantities
    :param user_id: int, ID of
    :return: dict of id:product_info_dict pairs
    """
    all_products = {}
    products = UserProduct.query \
        .filter_by(user_id=user_id) \
        .join(Product) \
        .add_columns(UserProduct.uuid, Product.name, Product.price, Product.store, Product.priority) \
        .all()
    for product in products:
        # TODO: could be optimized by doing the aggregation inside the query
        quantities = Quantity.query.filter_by(user_product_id=product.uuid).all()
        id = product.UserProduct.product_id
        all_products[id] = {"name": product.name,
                            "quantity": sum((qty.quantity for qty in quantities), 0),
                            "price": float(product.price),
                            "store": product.store,
                            "priority": product.priority}

    return all_products


def update_product(product_info: dict):
    """
    Will be pretty unintuitive given the other available metohds, so maybe don't use it at all
    :param product_id:
    :param product_info:
    :param user_id:
    :return:
    """
    product_id = product_info['uuid']
    product = Product.query.filter_by(uuid=product_id).first()
    if product:
        if product.last_update_timestamp < product_info['last_update_timestamp']:
            product.name = product_info['name']
            # TODO: this will need to be changed in the near future
            product.price = product_info['price']
            product.store = product_info['store']
            product.priority = product_info['priority']
            product.last_update_timestamp = product_info['last_update_timestamp']
            try:
                db.session.commit()
                return product.to_dict()
            except IntegrityError as e:
                db.session.rollback()
                raise e
    else:
        raise NoSuchProductError


def update_priority(product_id, priority) -> (dict, int):
    """
    Performs an atomic update to priority of product with `product_id`
    :param user_id:
    :param product_id: int product id
    :param device_id: str, android_id of a device
    :param quantity_delta: int amount by which the product stock will change
    :param overwrite: bool, if True, existing delta will be overwritten with new data
    :return: tuple (response_json, status_code)
    """
    #     user_id = user_id or get_user_id_from_auth()
    # user_product = UserProduct.query.filter_by(user_id=user_id, product_id=product_id).first()

    product = Product.query.filter_by(uuid=product_id).first()
    if product:
        product.priority = priority
        try:
            db.session.commit()
            return product.priority
        except IntegrityError as e:
            db.session.rollback()
            raise e
    else:
        raise NoSuchProductError
