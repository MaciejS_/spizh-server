import flask
from flask import request

from spizh.db.model.user import User


def get_user_id_from_auth():
    return User.query.filter_by(name=request.authorization.username).first().uuid


def make_json_response(response_body: dict, status_code: int, additional_headers: dict = None):
    response = flask.jsonify(response_body)
    response.status_code = status_code
    if additional_headers:
        for header, value in additional_headers.items():
            response.headers[header] = value
    return response