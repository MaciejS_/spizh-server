-- ALTER TABLE product ADD COLUMN update_timestamp INTEGER NOT NULL DEFAULT 0;
--
create table product_price
(
	uuid VARCHAR(36) not null primary key DEFAULT (lower(hex(randomblob(4))) || '-' ||
																								 lower(hex(randomblob(2))) || '-' ||
																									lower(hex(randomblob(2))) || '-' ||
																									lower(hex(randomblob(2))) || '-' ||
																									lower(hex(randomblob(6)))),
	product_id VARCHAR(36)
		references product (uuid)
			on delete cascade,
	price NUMERIC not null,
	store VARCHAR(100) not null,
	last_update_timestamp INTEGER default 0 not null
)
;

insert into main.product_price (product_id, price, store)
	SELECT product.uuid, product.price, product.store
	from main.product;

-- drop table main.product_price;