from uuid import uuid4

from sqlalchemy.orm import relationship

from spizh.extensions import db


class UserProduct(db.Model):
    __tablename__ = 'user_product'

    uuid = db.Column(db.String(36), primary_key=True)
    user_id = db.Column(db.String(36), db.ForeignKey("user.uuid", ondelete="CASCADE"))
    product_id = db.Column(db.String(36), db.ForeignKey("product.uuid", ondelete="CASCADE"))

    quantity = relationship("Quantity", cascade="delete")

    def __init__(self, user_id, product_id):
        db.Model.__init__(self,
                          uuid=str(uuid4()),
                          product_id=product_id,
                          user_id=user_id)