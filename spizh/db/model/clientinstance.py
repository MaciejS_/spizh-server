from datetime import datetime
from uuid import uuid4

from sqlalchemy.orm import relationship

from spizh.extensions import db


class ClientInstance(db.Model):
    """
    Because a single client may have multiple devices, and a single deivce might be used by many clients,
    an artificial identifier is required
    """
    __tablename__ = 'client_instance'

    uuid = db.Column(db.String(36), primary_key=True)
    device_id = db.Column(db.String(36), nullable=False)
    user_id = db.Column(db.String(36), db.ForeignKey("user.uuid"), nullable=False)

    last_sync_token = db.Column(db.String, default=None)
    register_timestamp = db.Column(db.DateTime, default=datetime.utcnow)

    unique_device_user_pair = db.UniqueConstraint(device_id, user_id)

    quantity = relationship("Quantity", cascade="delete")

    def __init__(self, device_id: str, user_id: str):
        db.Model.__init__(self,
                          uuid=str(uuid4()),
                          device_id=device_id,
                          user_id=user_id)
