from uuid import uuid4

from spizh.extensions import db


class Quantity(db.Model):
    __tablename__ = 'quantity'

    uuid = db.Column(db.String(36), primary_key=True)
    user_product_id = db.Column(db.String(36), db.ForeignKey("user_product.uuid"), nullable=False)
    client_instance_id = db.Column(db.String(36), db.ForeignKey("client_instance.uuid"), nullable=False)
    quantity = db.Column(db.Integer, default=0)

    unique_device_product_combination = db.UniqueConstraint(user_product_id, client_instance_id)

    def __init__(self, user_product_id, client_instance_id, quantity=0):
        db.Model.__init__(self,
                          uuid=str(uuid4()),
                          user_product_id=user_product_id,
                          client_instance_id=client_instance_id,
                          quantity=quantity)
