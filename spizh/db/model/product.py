from uuid import uuid4

from sqlalchemy.orm import relationship

from spizh.db.model.product_price import ProductPrice
from spizh.extensions import db


class Product(db.Model):
    __tablename__ = 'product'

    uuid = db.Column(db.String(36), primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    price = db.Column(db.Numeric, nullable=False)
    store = db.Column(db.String(100), nullable=False)
    is_removed = db.Column(db.Boolean, nullable=False, default=False)
    priority = db.Column(db.Integer, default=0)
    last_update_timestamp = db.Column(db.Integer, default=0, name="update_timestamp")

    user_product = relationship("UserProduct", cascade="delete")
    product_prices = relationship(ProductPrice, cascade="delete")

    def __init__(self, name, price, store, uuid=None, priority=0, last_update_timestamp=0):
        db.Model.__init__(self,
                          uuid=uuid or str(uuid4()),
                          name=name,
                          price=price,
                          store=store,
                          priority=priority,
                          last_update_timestamp=last_update_timestamp)

    def mark_as_removed(self):
        self.is_removed = True

    def to_dict(self) -> dict:
        return {"uuid": self.uuid,
                "name": self.name,
                "price": float(self.price),
                "store": self.store,
                "priority": int(self.priority),
                "last_update_timestamp": int(self.last_update_timestamp)}
