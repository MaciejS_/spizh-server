from datetime import datetime
from uuid import uuid4

from sqlalchemy.orm import relationship

from spizh.extensions import db


class User(db.Model):
    __tablename__ = 'user'

    uuid = db.Column(db.String(36), primary_key=True)
    name = db.Column(db.String(32), nullable=False, unique=True)
    auth = db.Column(db.String(64), nullable=False)
    join_date = db.Column(db.DateTime, default=datetime.utcnow)

    user_product = relationship("UserProduct", cascade="delete")

    def __init__(self, name, auth):
        db.Model.__init__(self,
                          uuid=str(uuid4()),
                          name=name,
                          auth=auth)

    def to_dict(self):
        return {"name": self.name,
                "auth": self.auth,
                "uuid": self.uuid,
                "join_date": self.join_date}