from uuid import uuid4

from spizh.extensions import db


class ProductPrice(db.Model):
    __tablename__ = 'product_price'

    uuid = db.Column(db.String(36), primary_key=True)
    product_id = db.Column(db.String(36), db.ForeignKey("product.uuid", ondelete="CASCADE"))
    price = db.Column(db.Numeric, nullable=False)
    store = db.Column(db.String(100), nullable=False)
    last_update_timestamp = db.Column(db.Integer, nullable=False)
    is_removed = db.Column(db.Boolean, nullable=False, default=False)

    unique_price_combination = db.UniqueConstraint(product_id, store)

    def __init__(self, product_id, price, store, uuid=None, last_update_timestamp=0, is_removed=False):
        db.Model.__init__(self,
                          uuid=uuid or str(uuid4()),
                          product_id=product_id,
                          price=price,
                          store=store,
                          last_update_timestamp=last_update_timestamp,
                          is_removed=is_removed)

    def to_dict(self) -> dict:
        return {"uuid": self.uuid,
                "product_id": self.product_id,
                "price": float(self.price),
                "store": self.store,
                "last_update_timestamp": self.last_update_timestamp,
                "is_removed": self.is_removed}
