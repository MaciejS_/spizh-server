import os

from flask import Flask

from spizh.api import api_v1
from spizh.api import api_v2
from spizh.extensions import db

app = Flask(__name__)
# app.config['database_file'] = os.path.join("C:\\tmp\\spizh\\", 'spizh.db')
app.config['database_file'] = os.path.join("C:/Users/macsakow/Git/spizh/server_instance/", 'spizh.db')
app.config["SQLALCHEMY_DATABASE_URI"] = 'sqlite:///' + app.config['database_file']

app.register_blueprint(api_v1)
app.register_blueprint(api_v2)
db.init_app(app)


@app.cli.command('create_db')
def create_test_database():
    with app.app_context():
        db.create_all()
    print("Database created")


@app.cli.command('remove_db')
def remove_db():
    choice = input("This action will remove the database, are you sure you want to proceed? [Y/n]")
    if choice == "Y":
        os.remove(app.config['database_file'])
        print("Database removed")

from flask import current_app

@app.cli.command('list_urls')
def urls():
    """Display all of the url matching routes for the project.

    Borrowed from Flask-Script, converted to use Click.
    """
    rows = []
    column_length = 0
    column_headers = ('Rule', 'Endpoint', 'Arguments')

    rules = sorted(
        current_app.url_map.iter_rules(),
        key=lambda rule: getattr(rule, 'rule'))
    for rule in rules:
        rows.append((rule.rule, rule.endpoint, None))
    column_length = 2

    str_template = ''
    table_width = 0

    if column_length >= 1:
        max_rule_length = max(len(r[0]) for r in rows)
        max_rule_length = max_rule_length if max_rule_length > 4 else 4
        str_template += '{:' + str(max_rule_length) + '}'
        table_width += max_rule_length

    if column_length >= 2:
        max_endpoint_length = max(len(str(r[1])) for r in rows)
        # max_endpoint_length = max(rows, key=len)
        max_endpoint_length = (
            max_endpoint_length if max_endpoint_length > 8 else 8)
        str_template += '  {:' + str(max_endpoint_length) + '}'
        table_width += 2 + max_endpoint_length

    if column_length >= 3:
        max_arguments_length = max(len(str(r[2])) for r in rows)
        max_arguments_length = (
            max_arguments_length if max_arguments_length > 9 else 9)
        str_template += '  {:' + str(max_arguments_length) + '}'
        table_width += 2 + max_arguments_length

    print(str_template.format(*column_headers[:column_length]))
    print('-' * table_width)

    for row in rows:
        print(str_template.format(*row[:column_length]))


@app.teardown_appcontext
def close_db(error):
    db.session.remove()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port='7447')



